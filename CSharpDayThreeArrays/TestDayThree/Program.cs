﻿using System;
using System.Linq.Expressions;

public class CSharpDayThree
{
    static void Main()
    {}
    public int FindMissingNumber(int[] array)
    {
        if(array.Length < 2)
        {
            return 0;
        }
        int min = array[0];
        int[] bucketArray = new int[array.Length]; 
        int max = array[array.Length - 1];
        int result=0;
        int realSum = 0;
        for (int i = 0; i < array.Length; i++)
        {
            realSum+=array[i];
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];        
            }
        }
        int sum=(min+max)*(array.Length+1)/2;

        return sum-realSum;
    }
    public int CubedRootOfInt(int num)
    {
        int length = num.ToString().Length;
        int startBase=0;
        int endBase = 0;
        int middleBase=0;
        if (length > 3 && length<7)
        {
                startBase = 10;
                endBase = 100;
                middleBase =50;
            }
        else if(length<4)
        {
            startBase = 1;
            middleBase = 5;
            endBase = 10;
        }
        else if (length > 6)
        {
            startBase = 100;
            endBase = 1000;
            middleBase = 500;
        }
        int cubeOfMiddle = middleBase * middleBase * middleBase;
        if (cubeOfMiddle <= num)
        {
            if(cubeOfMiddle == num)
            {
                return middleBase;
            }
           int secondMiddleBase=7*startBase;
            cubeOfMiddle = secondMiddleBase * secondMiddleBase * secondMiddleBase;
            if (cubeOfMiddle <= num)
            {
                for(int i = secondMiddleBase; i <= endBase; i++)
                {
                    if (i * i * i == num)
                    {
                        return i;
                    }
                    
                }
                return 0;
            }
            else if(cubeOfMiddle > num)
            {
                for (int i = secondMiddleBase; i >middleBase ; i--)
                {
                    if (i * i * i == num)
                    {
                        return i;
                    }
                }
                return 0;
            }
        }
        else if (cubeOfMiddle > num)
        {
            int secondMiddleBase = 3 * (length - 2);
            cubeOfMiddle = secondMiddleBase * secondMiddleBase * secondMiddleBase;
            if(cubeOfMiddle <= num)
            {
                for (int i = secondMiddleBase; i <= middleBase; i++)
                {
                    if (i * i * i == num)
                    {
                        return i;
                    }
                }
                return 0;
            }
            else if(cubeOfMiddle > num)
            {
                for (int i = startBase; i < secondMiddleBase; i++)
                {
                    if (i * i * i == num)
                    {
                       return i;
                    }
                }
                return 0;
            }
        }
        return 0;
    }
}
