namespace TestingCSharpDayThree
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MissingnumberRandomNumbers()
        {
            //Arrange
            int[] testArray = { 121, 122, 123, 125, 126, 127, 128 };
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 124;
            //Act
            int result = tested.FindMissingNumber(testArray);
            //Assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void MissingNumberSingleNumber()
        {
            //Arrange
            int[] testArray = { 121 };
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 0;
            //Act
            int result = tested.FindMissingNumber(testArray);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestingCubedRootRandomNumber()
        {
            //Arrange
            int testInt = 125;
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 5;
            //Act
            int result = tested.CubedRootOfInt(testInt);
            //Assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void TestingCubedRootTwoDigitNumber()
        {
            //Arrange
            int testInt = 1331;
            CSharpDayThree tested = new CSharpDayThree();
            int expected = 11;
            //Act
            int result = tested.CubedRootOfInt(testInt);
            //Assert
            Assert.AreEqual(expected, result);
        }
    }
}