﻿public class CSharpDayFour
{
    static void Main()
    {
        List<int> numbers = new List<int> { 11, 11, 11, 11, 12, 11, 11, 11, 11 };
        CSharpDayFour test = new CSharpDayFour();
        Console.WriteLine(test.UniqueElementsOfList(numbers));
        Console.WriteLine(test.RemoveMiddleListElement(numbers));
        string input = "HELL3(O)";
        Console.WriteLine(test.ExpressionExpansion(input));
    }
    public string UniqueElementsOfList(List<int> list)
    {
        List<int> uniqueElements = new List<int>();
        foreach (int element in list)
        {
            if (!uniqueElements.Contains(element))
            {
                uniqueElements.Add(element);
            }
        }
        return string.Join(", ", uniqueElements);
    }
    public string RemoveMiddleListElement(List<int> list)
    {
        string error = "List is too short!";
        if (list.Count < 3)
        {
            return error;
        }
        List<int> copiedHalf = new List<int>();
        int halfLength;
        if (list.Count % 2 == 0)
        {
            halfLength = list.Count / 2;
        }
        else
        {
            halfLength = (list.Count + 1) / 2;
        }
        list.Remove(list[halfLength]);
        return String.Join(", ", list);
    }
    public string ExpressionExpansion(string expr)
    {
        string newString = "";
        for (int i = 0; i < expr.Length; i++)
        {
            char c = expr[i];
            if (char.IsDigit(c))
            {
                int repeats = c - '0';
                int nested = i + 1;
                while (nested < expr.Length && expr[nested] != ')')
                {
                    nested++;
                }
                string subString = expr.Substring(i + 2, nested - i - 2);
                string expandedString = ExpressionExpansion(subString);
                for (int k = 0; k < repeats; k++)
                {
                    newString += expandedString;
                }
                i = nested;
            }
            else
            {
                if(c!=')')
                newString += c;
            }
        }
        return newString;
    }

}