﻿
using System.Globalization;

public class CSharpDayTwo
{
    static void Main(){
    }
    public bool FibonacciArray(int[,] matrix)
    {

        int numRowsCols = matrix.GetLength(0);
        if (numRowsCols != matrix.GetLength(1))
        {
           throw new ArgumentException("Now a square");
        }
        int elementsNum = 2 * (matrix.GetLength(0) + matrix.GetLength(1)-1);
        int[] elementsArray = new int[elementsNum];
        for (int i = numRowsCols - 1; i >= 0; i--)
        {
            elementsArray[numRowsCols - (i + 1)] = matrix[0, i];
            if (i > 0)
            {
                elementsArray[4 * numRowsCols - i - 3] = matrix[i - 1, numRowsCols - 1];
            }
        }
        for(int j = 1; j < numRowsCols; j++)
        {
            elementsArray[numRowsCols+j-1] = matrix[j,0];
            elementsArray[2*numRowsCols-2+j]=matrix[numRowsCols-1,j];
        }
        elementsArray[elementsArray.Length-1] = matrix[0,numRowsCols-2];
        for(int l = 1; l < elementsArray.Length - 1; l++)
        {
            int prevNums = elementsArray[l - 1] + elementsArray[l];
            if ( prevNums== elementsArray[l + 1])
            {
                return true;
            }
        }
        return false;
    }
    public bool FibonacciMatrix(int[,] matrix)
    {
        if (matrix.GetLength(1) < 3)
        {
            return false;
        }
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            int rowLength=matrix.GetLength(1);
            int[] rowElements = new int[rowLength];
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                rowElements[j] = matrix[i, j];
            }
            if (rowElements.Length > 2)
            {
                for (int k = 1; k < rowElements.Length - 1; k++)
                {
                    if (rowElements[k - 1] + rowElements[k] == rowElements[k + 1])
                    {
                        return true;
                    }
                }
            }
        }
            return false;
    }
    public int[] ArrangeIntArray(int[] array)
    {
        int[] bitsSumArray = new int[array.Length];
        int[] copyBitsSumArray = new int[array.Length];
        int[] rearangedArray = new int[array.Length];
        Array.Copy(array, rearangedArray, array.Length);
        foreach (int element in array)
        {
            string binaryString = Convert.ToString(element, 2);
            int binarySum = 0;
            foreach (char binaryDigit in binaryString)
            {
                if (binaryDigit == '1')
                {
                    binarySum++;
                }
            }
            int indexOfArray = Array.IndexOf(array, element);
            bitsSumArray[indexOfArray] = binarySum;
            copyBitsSumArray[indexOfArray] = binarySum;
        }
        Array.Sort(bitsSumArray, rearangedArray);
        return rearangedArray;
    }

}