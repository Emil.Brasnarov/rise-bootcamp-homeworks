namespace CSharpDayTwoTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FibonacciMatrixContainsSequence()
        {
            //Arrange
            CSharpDayTwo test=new CSharpDayTwo();
            int[,] matrix = new int[3, 4] { { 1, 2, 11, 2 }, { 0, 2, 2, 3 }, { 0, 1, 1, 2 } };
            //Act
            bool result = test.FibonacciMatrix(matrix);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void FibonacciMatrixDosentContainSequence()
        {
            //Arrange
            CSharpDayTwo test = new CSharpDayTwo();
            int[,] matrix = new int[3, 4] { { 1, 2, 11, 2 }, { 1, 2, 2, 3 }, { 0, 2, 1, 2 } };
            //Act
            bool result = test.FibonacciMatrix(matrix);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void FibonacciMatrixTooShortRows()
        {
            //Arrange
            CSharpDayTwo test = new CSharpDayTwo();
            int[,] matrix = new int[3, 2] { { 1, 2 }, {2, 3 }, {1, 2 } };
            //Act
            bool result = test.FibonacciMatrix(matrix);
            //Assert
            Assert.IsFalse(result);
        }
        [TestMethod]
        public void FibonacciTwoDArrayTrue()
        {
            //Arrange
            CSharpDayTwo test = new CSharpDayTwo();
            int[,] matrix = new int[4, 4] {
            { 1, 1, 1, 1 },
            { 1, 1, 1, 1 },
            { 2, 1, 1, 1 },
            { 3, 5, 1, 1 },
            };
            //Act
            bool result = test.FibonacciArray(matrix);
            //Assert
            Assert.IsTrue(result);
        }
        [TestMethod]
        public void FibonacciTwoDArrayFalse()
        {
            //Arrange
            CSharpDayTwo test = new CSharpDayTwo();
            int[,] matrix = new int[4, 4] {
            { 1, 1, 1, 1 },
            { 1, 1, 1, 1 },
            { 5, 1, 1, 1 },
            { 1, 5, 1, 1 },
            };
            //Act
            bool result = test.FibonacciArray(matrix);
            //Assert
            Assert.IsFalse(result);
        }
    }
}
